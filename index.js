/*jslint node: true */

module.exports = (function () {
    'use strict';

    process.on('uncaughtException', function uncaughtException(err) {
        console.error(err.stack.split('\n').join(','));
    });

    var sc      = require('./config'),
        role    = process.env.ROLE || 'Analyzer';

    if (!module.parent) {
        sc
        .get('service.' + role)
        .then(
          console.log,
          function (err) { console.error(err.stack.split('\n').join(',')); }
        );
    }

    return sc;
}());
