Text Analizer
=============

Service to analize words stats into texts.

## testing the project

```
$ npm test
```

## running the service

```
$ npm install
$ npm run-script analize
```

By default take "enunciado.txt" as the default input, to define another set of files to analize run with:

```
$ export ANALYZER_FILES="coma,separated,filesnames,to,analize"
$ npm run-script analize
```

## Docker

### Build Docker image
```
$ docker build -t text-analizer .
```

### Run Docker image
```
$ docker run text-analizer
```

### Run Test into docker image
```
$ docker run text-analizer npm test
```
