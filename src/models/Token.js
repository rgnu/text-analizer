/*jslint node:true*/
'use strict';

class Token {

    constructor(token) {
      this.token = token;
    }

    toString() {
        return `Token:token=${this.token}`;
    }

}

module.exports = Token;
