/*jslint node:true*/
'use strict';

const Token = require('./Token');

class EnumerableToken extends Token {

    constructor(token, number) {
      super(token);

      this.number = number;
    }

    toString() {
        return `EnumerableToken:token=${this.token},number=${this.number}`;
    }
}

module.exports = EnumerableToken;
