'use strict';

const assert = require('assert');
const _      = require('highland');
const lib    = require('../');

function StopWordFilter(list) {

  list = list || []

  return function handler(obj) {
    return obj instanceof Object
      && typeof(obj.text) === 'string' 
      && list.indexOf(obj.text) < 0
  }
}

module.exports = StopWordFilter
