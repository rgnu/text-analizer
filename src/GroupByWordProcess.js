'use strict';

const assert = require('assert');
const _      = require('highland');


function GroupByWordProcess() {

  var wordGroup = {}

  return function handler(prev, obj) {

    assert(obj.text, '"text" property should be defined')

    if (typeof prev[obj.text] === 'undefined')
      prev[obj.text] = {"word": obj.text, "total-occurances": 0, "sentence-indexes": []}

    prev[obj.text]["total-occurances"]++
    prev[obj.text]["sentence-indexes"].push(obj.number)

    return prev
  }
}

module.exports = GroupByWordProcess;
