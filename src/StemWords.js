'use strict';

const natural = require('natural');
const _       = require('highland');

function StemWords() {

  var counter = 0;

  return function handler(obj) {
    return { number: obj.number || counter++, text: natural.PorterStemmer.stem(obj.text) }
  }
}

module.exports = StemWords;
