'use strict';

const natural = require('natural');
const _       = require('highland');

function ParagraphTokenizer(pattern) {

  pattern = pattern || /[.!?]+/

  var tokenizer = new natural.RegexpTokenizer({pattern: pattern});
  var counter   = 0;

  return function handler(obj) {

    obj = typeof(obj) === 'object' ? obj : { text: obj.toString()}

    return _(tokenizer.tokenize(obj.text))
      .filter(function notEmpty(token) {
        return token.length
      })
      .map(function map(token) {
        return { number: obj.number || counter++, text: token.trim() }
      })
  }
}

module.exports = ParagraphTokenizer;
