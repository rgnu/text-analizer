'use strict';

const natural = require('natural');
const _       = require('highland');

function WordTokenizer(pattern) {

  pattern = pattern || /[^A-Za-zА-Яа-я0-9_]+/

  const tokenizer = new natural.RegexpTokenizer({pattern: pattern});
  var counter     = 0;

  return function handler(obj) {

    obj = typeof(obj) === 'object' ? obj : { text: obj.toString()}

    return _(tokenizer.tokenize(obj.text))
      .filter(function notEmpty(token) {
        return token.length
      })
      .map( function map(word) {
        return { number: obj.number || counter++, text: word.trim() }
      })
  }
}

module.exports = WordTokenizer
