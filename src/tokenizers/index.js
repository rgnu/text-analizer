/*jslint node:true*/
'use strict';

module.exports.ParagraphTokenizer = require('./ParagraphTokenizer');
module.exports.WordTokenizer      = require('./WordTokenizer');
