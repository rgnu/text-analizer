'use strict';

const assert = require('assert');

function StringEncoder() {

  return function handler(obj) {
    assert(obj instanceof Object, 'input should be instance of Object');

    return JSON.stringify(obj, null, 2);
  }
}

module.exports = StringEncoder;
