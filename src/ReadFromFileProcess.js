'use strict';

const assert = require('assert');
const _      = require('highland');
const fs     = require('fs');

function ReadFromFileProcess() {

  return function handler(file) {
    assert(typeof(file) === 'string', 'input should be instance of String');

    return _(fs.createReadStream(file)).splitBy(/\r?\n/)
  }
}

module.exports = ReadFromFileProcess;
