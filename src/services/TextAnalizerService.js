/*jslint node:true*/
'use strict';

const lib = require('../');
const _   = require('highland')

class TextAnalizerService {

  constructor(files, stopWordsList) {
    this.files         = files;
    this.stopWordsList = stopWordsList;
  }

  run() {
    return _(this.files)
    .flatMap(lib.ReadFromFileProcess())
    .flatMap(lib.tokenizers.ParagraphTokenizer())
    .flatMap(lib.tokenizers.WordTokenizer())
    .filter(lib.filters.StopWordFilter(this.stopWordsList))
    .map(lib.StemWords())
    .reduce({}, lib.GroupByWordProcess())
    .map(lib.SortGroupedWordsProcess())
    .map(lib.StringEncoder())
  }

  toString() {
    return `TextAnalizerService:files=${this.files}`;
  }
}

module.exports = TextAnalizerService;
