'use strict';

const assert = require('assert');
const _      = require('highland');

function SortGroupedWordsProcess() {

  return function handler(result) {

    return { result: Object.keys(result)
      .map(function(key) {
        return result[key]
      })
      .sort(function(a, b) { return a.word < b.word ? -1 : a.word > b.word ? 1 : 0})
    }
  }
}

module.exports = SortGroupedWordsProcess;
