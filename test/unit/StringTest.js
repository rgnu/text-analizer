/*jslint node:true, mocha:true*/
'use strict';

const lib    = require('../../src');
const sinon  = require('sinon');
const should = require('chai').should();

describe('String', function() {

  before( function() {
      this.obj = "String Test";
  });

  it('Should have a method toLowerCase', function() {
      this.obj.toLowerCase.should.be.instanceof(Function);
  });

});
