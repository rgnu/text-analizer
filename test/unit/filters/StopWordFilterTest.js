/*jslint node:true, mocha:true*/
'use strict';

const lib    = require('../../../src');
const sinon  = require('sinon');
const chai   = require('chai');
const should = chai.should();

describe('StopWordFilter', function() {

  before( function() {
    this.obj = lib.filters.StopWordFilter();
  });

  it('Should be a Function', function() {
    this.obj.should.be.instanceof(Function);
  });

  provider([
    [{ input: {text: 'one'}, list: ['and', 'or'] }, { result: true }],
    [{ input: 1, list: ['and', 'or'] }, { result: false }],
    [{ input: null, list: ['and', 'or'] }, { result: false }]
  ],
  function(args, expect) {
    describe(`When call with "${args.input}"`, function() {

      var result;

      before(function() {
        this.obj = lib.filters.StopWordFilter(args.list);

        result   = this.obj(args.input);
      });

      it(`Then result should be "${expect.result}"`, function() {
        result.should.be.equal(expect.result);
      });

    });
  });
});
