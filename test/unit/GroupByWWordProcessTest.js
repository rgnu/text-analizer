/*jslint node:true, mocha:true*/
'use strict';

const lib    = require('../../src');
const sinon  = require('sinon');
const should = require('chai').should();

describe('GroupByWordProcess', function() {

  before( function() {
    this.obj = lib.GroupByWordProcess();
  });

  it('Should be a Function', function() {
    this.obj.should.be.instanceof(Function);
  });

  provider([
    [
      [ {text:'z', number: 1}, {text:'z', number: 2}, {text: 'a', number: 3} ],
      {
        "a": {
          "sentence-indexes": [3],
          "total-occurances": 1,
          "word": "a"
        },
        "z": {
          "sentence-indexes": [1, 2],
          "total-occurances": 2,
          "word": "z"
        }
      }
    ]
  ],
  function(input, expect) {
    describe(`When call with "${JSON.stringify(input)}"`, function() {

      var result;

      before(function() {
        result = input.reduce(this.obj, {})
      });

      it(`Then result should be equal to ${JSON.stringify(expect)}`, function() {
        result.should.be.deep.equal(expect);
      });
    });
  });
});
