/*jslint node:true, mocha:true*/
'use strict';

const lib    = require('../../src');
const sinon  = require('sinon');
const should = require('chai').should();

describe('SortGroupedWordsProcess', function() {

  before( function() {
    this.obj = lib.SortGroupedWordsProcess();
  });

  it('Should be a Function', function() {
    this.obj.should.be.instanceof(Function);
  });

  provider([
    [[{word:'z'}, {word:'a'}], { result: [{word:'a'}, {word:'z'}] } ]
  ],
  function(input, expect) {
    describe(`When call with "${JSON.stringify(input)}"`, function() {

      var result;

      before(function() {
        result = this.obj(input)
      });

      it(`Then result should be equal to ${JSON.stringify(expect)}`, function() {
        result.should.be.deep.equal(expect);
      });
    });
  });
});
