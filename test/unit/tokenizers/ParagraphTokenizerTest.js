/*jslint node:true, mocha:true*/
'use strict';

const lib    = require('../../../src');
const sinon  = require('sinon');
const should = require('chai').should();

describe('ParagraphTokenizer', function() {

  before( function() {
    this.obj = lib.tokenizers.ParagraphTokenizer();
  });

  it('Should be a Function', function() {
    this.obj.should.be.instanceof(Function);
  });

  provider([
    ['Test one. Test two? Test three!!', { length: 3, last: {number: 2, text: 'Test three'}}]
  ],
  function(args, expect) {
    describe(`When call with "${args}"`, function() {

      var result;

      before( function(done) {
        this.obj(args).toArray(function(it) {
          result = it
          done()
        })
      });

      it('Then result should be a Array', function() {
        result.should.be.instanceof(Array);
      });

      it(`And result have ${expect.length} items`, function() {
        result.length.should.be.equal(expect.length);
      });

      it(`And last value should be ${expect.last}`, function() {
        result.pop().should.be.deep.equal(expect.last);
      });
    });
  });
});
