/*jslint node:true*/

provider = function provider(cases, fn) {

    'use strict';

    cases.forEach(function (c, n) {
        fn.apply({}, c);
    });
};

addFields = function addFields(objs, fields) {
    list = Array.isArray(objs) && objs || [objs];

    list.forEach(function(o) {
        fields.forEach(function(k) { o[k] = 1; });
    });

    return Array.isArray(objs) && list || list[0];
};
