/*jslint node:true */

module.exports = function(sc) {

    'use strict';

    /**
     * Analyzer Service
     */
    sc.bind('service.Analyzer')
    .inject('highland', 'lib', 'config.analyzer')
    .to(function(_, lib, config) {

      return new Promise(function(resolve, reject) {
        var service = new lib.services.TextAnalizerService(config.Files, config.StopWords);

        service
        .run()
        .errors(reject)
        .each(resolve)

      });
    });

    return sc;
};
