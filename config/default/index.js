/*jslint node: true, nomen: true */
'use strict';

module.exports = function setup(sc) {
  /**
   * Include Config
   */
  sc.include([__dirname, 'config.js']);

  /**
   * Include Requires
   */
  sc.include([__dirname, 'requires.js']);

  /**
   * Include Services
   */
  sc.include([__dirname, 'services.js']);

  return sc;
};
