/*jslint node: true */
'use strict';

module.exports = function setup(sc) {
  /**
   * Requires
   */
  sc.bind('lib').toModule([__dirname, '..', '..', 'src']);

  sc.bind('highland').toModule('highland');

  return sc;
};
