/*jslint node:true */
'use strict';

module.exports = function config(sc) {
  /**
   * Analizer Config
   */
  sc.bind('config.analyzer.Files')
    .toInstance((process.env.ANALYZER_FILES || 'enunciado.txt').split(','));

  sc.bind('config.analyzer.StopWords')
    .toInstance(["a", "the", "and", "of", "in", "be", "also", "as"]);

  return sc;
};
