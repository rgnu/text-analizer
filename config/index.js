/*jslint node:true*/

module.exports = (function () {
    'use strict';

    var sc   = require('./container');

    sc.include([__dirname, 'env.js']);

    var env  = process.env.APP_ENVIRONMENT || process.env.NODE_ENV || 'production';

    sc.bind('config.Environment').toInstance(env);

    sc.include([__dirname, 'default']);
    sc.include([__dirname, env]);

    return sc;
}());
