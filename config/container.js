/*jslint node:true */
/*jshint -W030 */

module.exports = (function () {
    'use strict';

    var fs   = require('fs'),
        path = require('path'),
        di   = require('q-di');

    function Container() {
        this.config    = {};
        this.container = null;
    }

    Container.prototype.include = function include(file) {
        if (Array.isArray(file)) { file = path.join.apply(path, file); }
        if (fs.existsSync(file)) { require(file)(this); }
    };

    Container.prototype.bind = function bind(key) {
        var that = this;

        var api  = {};
        api.to = function to(fn) {

            //-> Handle References
            if (typeof fn === 'string') {
                return api.to(function() {
                    var args = arguments;
                    return that.get(fn)
                        .then(function(fn) {
                            return fn.apply(undefined, args);
                        });
                });
            }

            (typeof(that.config[key]) === 'object') &&
                (that.config[key].create = fn) ||
                (that.config[key] = fn);

            return that;
        };

        api.toInstance = function toInstance(instance) {
            api.to(function() { return instance; });
            return that;
        };

        api.toException = function toException(error) {
            api.to(function() { throw error; });
            return that;
        };

        api.toModule = function toModule(module) {
            if (Array.isArray(module)) { module = path.join.apply(path, module); }
            api.to(function() { return require(module); });
            return that;
        };

        api.toType = function toType(C) {

            function factory(klass) {
                return function() {
                    var o = Object.create(klass.prototype);
                    klass.apply(o, arguments);
                    return o;
                };
            }

            //-> Handle References
            if (typeof C === 'string') {
                return api.to(function() {
                    var args = arguments;
                    return that.get(C)
                        .then(function(klass) {
                            return factory(klass).apply(undefined, args);
                        });
                });
            }

            api.to(factory(C));

            return that;
        };

        api.inject = function inject() {
            that.config[key] = { args: arguments, create: function() {} };
            return api;
        };

        return api;
    };

    Container.prototype.get = function get(key, cb) {

        var result = null;

        (this.container) || (this.container = new di.Injector(this.config, { hierarchical: true }));

        if (!this.container[key]) throw new Error(key + ' can\'t be resolved');

        result = this.container[key]();

        (result && cb && result.then(cb));

        return result;
    };

    return new Container();
}());
